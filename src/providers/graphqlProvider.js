import buildGraphQLProvider from 'ra-data-graphql-simple';

const buildProvider = buildGraphQLProvider({ clientOptions: { uri: 'http://localhost:8080' }})
    .then(dataProvider);

export default buildProvider;