import buildGraphQLProvider, { buildQuery } from 'ra-data-graphql-simple';
import gql from 'graphql-tag';

 
const myBuildQuery = introspection => (fetchType, resource, params) => {
    const builtQuery = buildQuery(introspection)(fetchType, resource, params);
 
    if (resource === 'account_customers' && fetchType === 'GET_LIST') {
        return {
            // Use the default query variables and parseResponse
            ...builtQuery,
            // Override the query
            query: gql`
                query ($id: ID!) {
                    account_customers {
                        customer_id
                        email
                        name
                      }
                }`,
        };
    }
 
    return builtQuery;
}
 
export default myBuildQuery;
// export default buildGraphQLProvider({ buildQuery: myBuildQuery })