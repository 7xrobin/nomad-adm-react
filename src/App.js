import React, {Component} from 'react';
import { Admin, Resource, ListGuesser } from 'react-admin';
import CustomerList from './components/CustomerList';
import nomadProvider from './providers/nomadProvider';

const App = () => (
  <Admin
    dataProvider={nomadProvider}
 >
    <Resource
      name="account_customers"
      list={CustomerList}
    />
  </Admin>
);

export default App;